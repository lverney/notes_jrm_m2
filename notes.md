---
title: Notes on JRM
author: Lucas Verney
date: \today
header-includes:
    - \usepackage{dsfont}
    - \usepackage{mathtools}
    - \usepackage{stmaryrd}
    - \usepackage{cancel}
    - \renewcommand{\arraystretch}{1.5}
---
\tableofcontents

\pagebreak

## Standard Josephson Ring Modulator

![Standard JRM](figures/standard_JRM.svg)

We note $\varphi_k = \Phi_k / \varphi_0$ where $\varphi_0 = \hbar / 2e$.

We have

\begin{equation}
    \varphi_a + \varphi_b + \varphi_c + \varphi_d = \varphi_{\text{ext}} [2\pi]
\end{equation}

Let us define "normal" modes as

\begin{equation}
\begin{pmatrix} \varphi_X \\ \varphi_Y \\ \varphi_Z \\ \varphi_M \end{pmatrix} =
\begin{pmatrix}
    1/2 & -1/2 & -1/2 & 1/2 \\
    1/2 & 1/2 & -1/2 & -1/2 \\
    1/4 & -1/4 & 1/4 & -1/4 \\
    1 & 1 & 1 & 1
\end{pmatrix}
\begin{pmatrix} \varphi_a \\ \varphi_b \\ \varphi_c \\ \varphi_d \end{pmatrix}
\end{equation}

We have immediately that $\varphi_M = \varphi_{\text{ext}} [2\pi]$.


The hamiltonian of the system writes
\begin{equation}
H = -E_J \left( \cos \varphi_a + \cos \varphi_b + \cos \varphi_c + \cos \varphi_d \right)
\end{equation}

and

\begin{equation}
\begin{pmatrix} \varphi_a \\ \varphi_b \\ \varphi_c \\ \varphi_d \end{pmatrix} =
\begin{pmatrix}
    1/2 & 1/2 & 1 & 1/4 \\
    -1/2 & 1/2 & -1 & 1/4 \\
    -1/2 & -1/2 & 1 & 1/4 \\
    1/2 & -1/2 & -1 & 1/4
\end{pmatrix}
\begin{pmatrix} \varphi_X \\ \varphi_Y \\ \varphi_Z \\ \varphi_M \end{pmatrix}
\end{equation}


Then

\begin{equation}
\begin{split}
\cos \varphi_a + \cos \varphi_b + \cos \varphi_c + \cos \varphi_d \\
%
= & \cos \left( \frac{\varphi_X}{2} + \frac{\varphi_Y}{2} + \varphi_Z + \frac{\varphi_M}{4} \right) \\
  & + \cos \left( -\frac{\varphi_X}{2} + \frac{\varphi_Y}{2} - \varphi_Z + \frac{\varphi_M}{4} \right) \\
  & + \cos \left( -\frac{\varphi_X}{2} - \frac{\varphi_Y}{2} + \varphi_Z + \frac{\varphi_M}{4} \right) \\
  & + \cos \left( \frac{\varphi_X}{2} - \frac{\varphi_Y}{2} - \varphi_Z + \frac{\varphi_M}{4} \right)  \\
%
= & 2 \cos \left( \frac{\varphi_Y}{2} + \frac{\varphi_M}{4} \right) %
    \cos \left( \frac{\varphi_X}{2} + \varphi_Z \right) \\
  & + 2 \cos \left( -\frac{\varphi_Y}{2} + \frac{\varphi_M}{4} \right) %
    \cos \left( -\frac{\varphi_X}{2} + \varphi_Z \right) \\
%
= & 4 \cos \frac{\varphi_X}{2} \cos \frac{\varphi_Y}{2} \cos \varphi_Z \cos \frac{\varphi_M}{4} \\
  & + 4 \sin \frac{\varphi_X}{2} \sin \frac{\varphi_Y}{2} \sin \varphi_Z \sin \frac{\varphi_M}{4} \\
\end{split}
\end{equation}

Hence,
\begin{equation}
\boxed{
H =  -4E_J \left( \cos \frac{\varphi_X}{2} \cos \frac{\varphi_Y}{2} \cos \varphi_Z \cos \frac{\varphi_M}{4} %
  + \sin \frac{\varphi_X}{2} \sin \frac{\varphi_Y}{2} \sin \varphi_Z \sin \frac{\varphi_M}{4}\right)
}
\end{equation}
where
\begin{equation}
\boxed{\varphi_M = \varphi_{\text{ext}} + 2 \pi n}
\end{equation}


\pagebreak

## Shunted JRM

The shunted JRM is:

![Shunted JRM](figures/shunted_JRM.svg)


### Hamiltonian

As in the unshunted case, we define

\begin{equation}
\left\{\begin{array}{l}
\varphi_X = \frac{\varphi_a}{2} - \frac{\varphi_b}{2} - \frac{\varphi_c}{2} + \frac{\varphi_d}{2} \\
\varphi_Y = \frac{\varphi_a}{2} + \frac{\varphi_b}{2} - \frac{\varphi_c}{2} - \frac{\varphi_d}{2} \\
\varphi_Z = \frac{\varphi_a}{4} - \frac{\varphi_b}{4} + \frac{\varphi_c}{4} - \frac{\varphi_d}{4} \\
\end{array}\right.
\end{equation}


The circuit equations are:
\begin{equation}
\left\{\begin{array}{l}
\varphi_a - \varphi_{L_1} + \varphi_{L_4} = \frac{\varphi_{\text{ext}}}{4} + 2 \pi n_a \\
\varphi_b - \varphi_{L_4} + \varphi_{L_2} = \frac{\varphi_{\text{ext}}}{4} + 2 \pi n_b \\
\varphi_c - \varphi_{L_2} + \varphi_{L_3} = \frac{\varphi_{\text{ext}}}{4} + 2 \pi n_c \\
\varphi_d - \varphi_{L_3} + \varphi_{L_1} = \frac{\varphi_{\text{ext}}}{4} + 2 \pi n_d
\end{array}\right.
\end{equation}

where $\Phi_{\text{ext}} = \varphi_0 \varphi_{\text{ext}}$ is the magnetic
flux threading the area enclosed by the four junctions.

Writing the charge conservation in the middle of the circuit, we also have
\begin{equation}
\frac{\varphi_0}{L} \left( \varphi_{L_1} + \varphi_{L_2} + \varphi_{L_3} + \varphi_{L_4} \right) = 0
\end{equation}

as the current across the inductances is given by $\varphi_0 \varphi / L$.


The hamiltonian of this system is

\begin{equation}
\begin{split}
H = & -E_J \left( \cos \varphi_a + \cos \varphi_b + \cos \varphi_c + \cos \varphi_d \right) \\
    & + \frac{E_L}{2} \left( \varphi_{L_1}^2 + \varphi_{L_2}^2 + \varphi_{L_3}^2 + \varphi_{L_4}^2 \right)
\end{split}
\end{equation}

We also have

\begin{equation}
\varphi_M = \varphi_{\text{ext}} + 2 \pi \sum_k n_k
\end{equation}

So that, similarly to the unshunted case, the Josephson term of the
hamiltonian can be expressed as

\begin{equation}
E = -4E_J \left( \cos \frac{\varphi_X}{2} \cos \frac{\varphi_Y}{2} \cos \varphi_Z \cos \frac{\varphi_{\text{ext}}}{4} %
  + \sin \frac{\varphi_X}{2} \sin \frac{\varphi_Y}{2} \sin \varphi_Z \sin \frac{\varphi_{\text{ext}}}{4}\right)
\end{equation}

since $\sum_k n_k \in 4 \mathbb{Z}$. The degeneracy from the unshunted case is
now lifted.

*Note*: The fact that $\sum_k n_k \in 4 \mathbb{Z}$ can be seen easily by
looking at the hamiltonian in the sum of cosine form. All the $\varphi_k$ are
now $2\pi$-periodic, and we can then discard the $n_k$.


Let us now focus on the inductive part of the hamiltonian.

We can express the normal modes in terms of the fluxes on the inductances:

\begin{equation}
\left\{\begin{array}{l}
    \varphi_X = \varphi_{L_3} - \varphi_{L_4} \\
    \varphi_Y = \varphi_{L_1} - \varphi_{L_2} \\
    \varphi_Z = \frac{1}{2} \left( \varphi_{L_1} + \varphi_{L_2} - \varphi_{L_3} - \varphi_{L_4} \right)
\end{array}\right.
\end{equation}

From these equations, we get

\begin{equation}
\begin{split}
    4 \varphi_{L_1} & = 2 \varphi_Z + \varphi_{L_3} - \varphi_{L_2} + \varphi_{L_4} + 3 \varphi_{L_1} \\
                    & = 2 \varphi_Z + (\varphi_1 - \varphi_2) + \underbrace{(2 \varphi_1 + \varphi_3 + \varphi_4)}_{= \varphi_1 - \varphi_2} \\
                    & = 2 (\varphi_Y + \varphi_Z)
\end{split}
\end{equation}

Conversely, we get

\begin{equation}
\left\{\begin{array}{l}
4 \varphi_{L_2} = 2 (\varphi_Z - \varphi_Y) \\
4 \varphi_{L_3} = 2 (\varphi_X - \varphi_Z) \\
4 \varphi_{L_4} = -2 (\varphi_X + \varphi_Z)
\end{array} \right.
\end{equation}


Finally, for the inductive part,
\begin{equation}
\begin{split}
\varphi_{L_1}^2 + \varphi_{L_2}^2 + \varphi_{L_3}^2 + \varphi_{L_4}^2 %
    & = \frac{1}{4} \left[ (\varphi_Y + \varphi_Z)^2 + (\varphi_Z - \varphi_Y)^2 %
      + (\varphi_X - \varphi_Z)^2 + (\varphi_X + \varphi_Z)^2 \right] \\
    & = \frac{1}{4} \left[ 4 \varphi_Z^2 + 2 \varphi_X^2 + 2 \varphi_Y^2 \right] \\
    & = \frac{\varphi_X^2}{2} + \frac{\varphi_Y^2}{2} + \varphi_Z^2
\end{split}
\end{equation}


The total hamiltonian writes
\begin{equation}
\begin{split}
H = & -4E_J \left( \cos \frac{\varphi_X}{2} \cos \frac{\varphi_Y}{2} \cos \varphi_Z \cos \frac{\varphi_{\text{ext}}}{4} %
  + \sin \frac{\varphi_X}{2} \sin \frac{\varphi_Y}{2} \sin \varphi_Z \sin \frac{\varphi_{\text{ext}}}{4}\right) \\
    & + \frac{E_L}{2} \left[\frac{\varphi_X^2}{2} + \frac{\varphi_Y^2}{2} + \varphi_Z^2 \right]
\end{split}
\end{equation}

**TODO: Why not $n_X$, $n_Y$ and $n_Z$ as in Flavius?**


*Note*: Not crossing the JRM results in similar equations, but they do not
simplify when written using the normal modes.

![Uncrossed shunted JRM](figures/uncrossed_shunted_JRM.svg)

The inductive part does no longer write

$$
\varphi_1^2 + \varphi_2^2 + \varphi_3^2 + \varphi_4^2 =
    \varphi_X^2 + \varphi_Y^2 + 2 \varphi_Z^2
$$

but is a coupled combination of $\varphi_X$, $\varphi_Y$ and $\varphi_Z$.


### Quantization

In the small phases limit, we can expand the hamiltonian to
the third order:

\begin{equation}
\begin{split}
    H = & -4 E_J \cos \frac{\varphi_{\text{ext}}}{4} \\
        & + \frac{1}{2} \left(\frac{E_L}{2} + E_J \cos \frac{\varphi_{\text{ext}}}{4} \right) \left( \varphi_X^2 + \varphi_Y^2 \right) \\
        & + \frac{1}{2} \left(E_L + 4 E_J \cos \frac{\varphi_{\text{ext}}}{4} \right) \varphi_Z^2 \\
        & - E_J \sin \frac{\varphi_{\text{ext}}}{4} \varphi_X \varphi_Y \varphi_Z \\
        & + O(|\varphi|^4)
\end{split}
\end{equation}

We can now quantize the fluxes, introducing:

\begin{equation}
\left\{\begin{array}{l}
    \hat{\varphi_X} = \varphi_X^{0} \left( a + a^{\dagger} \right)\\
    \hat{\varphi_Y} = \varphi_Y^{0} \left( b + b^{\dagger} \right)\\
    \hat{\varphi_Z} = \varphi_Z^{0} \left( c + c^{\dagger} \right)\\
\end{array}\right.
\end{equation}

We now take a minimal circuit to couple the JRM to modes, using inductances
and capacitors. The quadratic part (due to inductances, capacitors and
quadratic part of the Josephson term) in the hamiltonian gives three harmonic
oscillators, up to a renormalization of frequencies. Then,

\begin{equation}
    H = \hbar \omega_a a^{\dagger}a %
        + \hbar \omega_b b^{\dagger} b %
        + \hbar \omega_c c^{\dagger} c %
        + \hbar \chi \left( a + a^{\dagger} \right) \left( b + b^{\dagger} \right) \left( b + b^{\dagger} \right)
\end{equation}

![Minimal circuit around the JRM](figures/minimal_circuit_JRM.svg)

\begin{equation}
    H = \underbrace{\hbar \omega_a a^{\dagger}a + \hbar \omega_b b^{\dagger} b + \hbar \omega_c c^{\dagger} c}_{H_0} %
        + \underbrace{\hbar \chi}_{u_k} \left( %
            \underbrace{abc + abc^{\dagger} + ab^{\dagger}c + ab^{\dagger}c^{\dagger} %
            + a^{\dagger}bc + a^{\dagger}bc^{\dagger} + a^{\dagger}b^{\dagger}c + a^{\dagger}b^{\dagger}c^{\dagger}}_{H_k} %
        \right)
\end{equation}

Using interaction representation with respect to $H_0$ (see _Dynamics and
Control of Open Quantum Systems_, M. Mirrahimi and P. Rouchon), we have

\begin{equation}
\begin{split}
    H_{int} = \hbar \chi ( & %
        e^{-i\hbar (\omega_a + \omega_b + \omega_c) t} abc + %
        e^{-i\hbar (\omega_a + \omega_b - \omega_c) t} abc^{\dagger} + %
        e^{-i\hbar (\omega_a - \omega_b + \omega_c) t} ab^{\dagger}c \\
        & + e^{-i\hbar (\omega_a - \omega_b - \omega_c) t} ab^{\dagger}c^{\dagger} + %
        e^{-i\hbar (-\omega_a + \omega_b + \omega_c) t} a^{\dagger}bc + %
        e^{-i\hbar (-\omega_a + \omega_b - \omega_c) t} a^{\dagger}bc^{\dagger} \\
        & + e^{-i\hbar (-\omega_a - \omega_b + \omega_c) t} a^{\dagger}b^{\dagger}c + %
        e^{-i\hbar (-\omega_a - \omega_b - \omega_c) t} a^{\dagger}b^{\dagger}c^{\dagger}
    )
\end{split}
\end{equation}

since $e^{i H_0 t} a e^{-i H_0 t} = e^{-i \hbar \omega_a t} a$, using
$a f(a^{\dagger} a) = f(a^{\dagger} a + \mathds{1}) a$ for any analytical
function $f$.

If we choose $\omega_c = \omega_a + \omega_b$, we get
\begin{equation}
    \overline{H_{int}} = \hbar \chi \left( a b c^{\dagger} + a^{\dagger} b^{\dagger} c \right)
\end{equation}


### Beyond third order

If we develop to the fourth order, we get additional "Kerr" terms. The
additional "Kerr" hamiltonian is

\begin{equation}
    H_{Kerr} = \left(1 - \frac{1}{2} \left( \frac{\varphi_X}{2} \right)^2 + \frac{1}{4!} \left( \frac{\varphi_X}{2} \right)^4 \right) + %
               \left(1 - \frac{1}{2} \left( \frac{\varphi_Y}{2} \right)^2 + \frac{1}{4!} \left( \frac{\varphi_Y}{2} \right)^4 \right) + %
               \left(1 - \frac{1}{2} \varphi_Z^2 + \frac{1}{4!} \varphi_Z^4 \right)
\end{equation}

When developping and using the RWA, the only remaining terms are terms with
equal power in $a$ (resp. $b$, $c$) and $a^{\dagger}$ (resp. $b^{\dagger}$,
$c^{\dagger}$). Up to a renormalization of the modes frequencies (possible if
the pump amplitude $c^{\dagger}c$ is kept constant), we get

\begin{equation}
    H_{Kerr} = \hbar K_{aa} (a^{\dagger} a)^2 + \hbar K_{bb} (b^{\dagger} b)^2 + %
               \hbar K_{cc} (c^{\dagger} c)^2 + \hbar K_{ab} (a^{\dagger} a b^{\dagger} b) + %
               \hbar K_{ac} (a^{\dagger} a c^{\dagger} c) + \hbar K_{bc} (b^{\dagger} b c^{\dagger} c)
\end{equation}


*Note*: If we treat the pump mode $c$ classically, we can replace $c$ (resp.
$c^{\dagger}$) by a complex value $p$ (resp. $p^*$).


\pagebreak

## Asymmetric shunted JRM

![Asymmetric shunted JRM](figures/asymmetric_shunted_JRM.svg)


### Hamiltonian

In this case, we keep the same definition for the normal modes:

\begin{equation}
\left\{\begin{array}{l}
    \varphi_X = \varphi_{L_3} - \varphi_{L_4} \\
    \varphi_Y = \varphi_{L_1} - \varphi_{L_2} \\
    \varphi_Z = \frac{1}{2} \left( \varphi_{L_1} + \varphi_{L_2} - \varphi_{L_3} - \varphi_{L_4} \right)
\end{array}\right.
\end{equation}


*Note*: In this case, inverting the normal modes definition does not work
anymore.
\begin{equation}
    \frac{\varphi_X}{2} + \frac{\varphi_Y}{2} + \varphi_Z + \frac{\varphi_M}{4} = \varphi_{L_1} - \varphi_{L_4} + \frac{\varphi_M}{4}
\end{equation}
is incompatible with the asymmetry of the loops (write Kirchhoff law and
compare results).


The circuit equations are

\begin{equation}
\left\{\begin{array}{l}
    \varphi_a - \varphi_{L_1} + \varphi_{L_4} = \frac{3 \varphi_{\text{ext}}}{4} + 2 \pi n_a \\
    \varphi_b - \varphi_{L_4} + \varphi_{L_2} = \frac{\varphi_{\text{ext}}}{4} + 2 \pi n_b \\
    \varphi_c - \varphi_{L_2} + \varphi_{L_3} = \frac{3 \varphi_{\text{ext}}}{4} + 2 \pi n_c \\
    \varphi_d - \varphi_{L_3} + \varphi_{L_1} = \frac{\varphi_{\text{ext}}}{4} + 2 \pi n_d
\end{array}\right.
\end{equation}

where this time $\varphi_{\text{ext}}$ is the magnetic flux threading a small
loop.

The hamiltonian still writes
\begin{equation}
\begin{split}
H = & -E_J \left( \cos \varphi_a + \cos \varphi_b + \cos \varphi_c + \cos \varphi_d \right) \\
    & + \frac{E_L}{2} \left( \varphi_{L_1}^2 + \varphi_{L_2}^2 + \varphi_{L_3}^2 + \varphi_{L_4}^2 \right)
\end{split}
\end{equation}

and its inductive part is given by the same expression as the symmetric shunted
case

\begin{equation}
\varphi_{L_1}^2 + \varphi_{L_2}^2 + \varphi_{L_3}^2 + \varphi_{L_4}^2 %
    = \frac{\varphi_X^2}{2} + \frac{\varphi_Y^2}{2} + \varphi_Z^2
\end{equation}


However, the Josephson part has a very different expression from the symmetric
case.

\begin{equation}
\begin{split}
\cos \varphi_a + \cos \varphi_b + \cos \varphi_c + \cos \varphi_d = & %
      \cos \left(\frac{3\pi}{4} + \frac{\varphi_X + \varphi_Y + 2\varphi_Z}{2} \right) \\
      & + \cos \left(\frac{\pi}{4} - \frac{\varphi_X - \varphi_Y + 2\varphi_Z}{2} \right) \\
      & + \cos \left(\frac{3\pi}{4} - \frac{\varphi_X + \varphi_Y - 2\varphi_Z}{2} \right) \\
      & + \cos \left(\frac{\pi}{4} + \frac{\varphi_X - \varphi_Y - 2\varphi_Z}{2} \right) \\
%
    = & 2 \cos \left( \frac{\pi}{2} + \frac{\varphi_Y}{2} \right) \cos \left( \frac{\pi}{4} + \frac{\varphi_X + \varphi_Z}{2} \right) \\
      & + 2 \cos \left( \frac{\pi}{2} - \frac{\varphi_Y}{2} \right) \cos \left( \frac{\pi}{4} - \frac{\varphi_X + 2 \varphi_Z}{2} \right) \\
%
    = & -2 \sin \frac{\varphi_Y}{2} \left[ \cos \left( \frac{\pi}{4} + \frac{\varphi_X - 2 \varphi_Z}{2} \right) - \cos \left( \frac{\pi}{4} - \frac{\varphi_X + 2 \varphi_Z}{2} \right)\right] \\
%
    = & 4 \sin \frac{\varphi_Y}{2} \sin \left( \frac{\pi}{4} - \varphi_Z \right) \sin \left( \frac{\varphi_X}{2} \right) \\
%
    = & 4 \frac{\sqrt{2}}{2} \sin \frac{\varphi_X}{2} \sin \frac{\varphi_Y}{2} \left[ \cos \varphi_Z - \sin \varphi_Z \right] \\
%
    = & 2 \sqrt{2} \sin \frac{\varphi_X}{2} \sin \frac{\varphi_Y}{2} \left[ \cos \varphi_Z - \sin \varphi_Z \right]
\end{split}
\end{equation}


Finally, the total hamiltonian is
\begin{equation}
\begin{split}
H = & -2 \sqrt{2} E_J \sin \frac{\varphi_X}{2} \sin \frac{\varphi_Y}{2} \left[ \cos \varphi_Z - \sin \varphi_Z \right] \\
    & + \frac{E_L}{4} \left[\varphi_X^2 + \varphi_Y^2 + 2 \varphi_Z^2 \right]
\end{split}
\end{equation}

*Note*: The term $\cos \varphi_Z - \sin \varphi_Z$ is a difference and not a
sum, contrary to _Mirrahimi et al., NJP 2014_, because we have opposed
definitions $\varphi_X$ and $\varphi_Y$ but the same definition for
$\varphi_Z$.


*Note*: If one takes a generic $\lambda$ factor instead of a 3 factor in the
loop surface difference, the Josephson part of the hamiltonian is
\begin{equation}
    E = -4 E_J \sin \frac{\varphi_X}{2} \sin \frac{\varphi_Y}{2} \left[ \sin \left( \frac{\lambda - 1}{2(\lambda + 1)} \right) \cos \varphi_Z - \cos \left( \frac{\lambda - 1}{2(\lambda + 1)} \right) \sin \varphi_Z \right]
\end{equation}

*Note*: Changing the geometry of the alternance of big and small loops, we can
tweak which mode will be the mode with a cos. By putting the two big loops at the
bottom of the circuit, for instance, the mode with a cos will be the $X$ mode.

*Note*: Removing a symmetry from the system, by using two loops of area 1, one
loop of area $\lambda$ and one loop of area $\lambda'$, the computation
becomes really cumbersome. However, in the limit $\lambda' = \lambda + \delta
\lambda$, one gets the same hamiltonian as before plus an extra term in
\begin{equation}
-\delta \lambda \frac{2\pi}{\lambda + \lambda' + 2} \sin \left(\frac{2\pi}{\lambda +
\lambda' + 2}  + \varphi \right)
\end{equation}
where $\varphi$ is the flux on the Josephson junction in the largest loop.

*Note*: This hamiltonian has a simple expression only in the case
$\varphi_{ext} = 2\pi$. Then, we have to fix the value of the external flux
and in the general case the hamiltonian cannot be put as a simple product of
cos and sin functions.


### Quantization

Let us linearize the new term in the limit of small fluxes. We get

\begin{equation}
\begin{split}
    \sin \left( \frac{\varphi_X}{2} \right) \sin \left( \frac{\varphi_Y}{2} \right) \left[ \cos \varphi_Z - \sin \varphi_Z \right] = & %
            \frac{\varphi_X}{2} \frac{\varphi_Y}{2} - %
            \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z - %
            \frac{1}{3!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^3 \\
%
        & - \frac{1}{3!} \left( \frac{\varphi_X}{2} \right)^3 \frac{\varphi_Y}{2} - %
            \frac{1}{2} \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z^2 \\
%
        & + \frac{1}{3!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^3 \varphi_Z + %
            \frac{1}{3!} \left( \frac{\varphi_X}{2} \right)^3 \frac{\varphi_Y}{2} \varphi_Z \\
%
        & + \frac{1}{3!} \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z^3 + %
            \frac{1}{(3!)^2} \left( \frac{\varphi_X}{2} \right)^3 \left( \frac{\varphi_Y}{2} \right)^3 \\
%
        & + \frac{1}{5!} \left( \frac{\varphi_X}{2} \right)^5 \frac{\varphi_Y}{2} + %
            \frac{1}{5!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^5 \\
%
        & + \frac{1}{2 \times 3!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^3 \varphi_Z^2 + %
            \frac{1}{2 \times 3!} \left( \frac{\varphi_X}{2} \right)^3 \frac{\varphi_Y}{2} \varphi_Z^2 \\
%
        & + \frac{1}{4!} \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z^4
\end{split}
\end{equation}

*Note*: As we impose a specific value of the external flux, we are in a very
specific case in which there is no contribution to the inductive part of the
hamiltonian from the non-linear term. Inductances associated to each mode are
simply given by the shunt inductances, for this JRM circuit at this working
point.

![Complete circuit of asymmetric shunted JRM](figures/asymmetric_shunted_JRM_circuit.svg)

By considering the whole circuit, we can introduce creation and annihilation operators for every mode,

\begin{equation}
\left\{\begin{array}{l}
    \hat{\varphi_X} = \varphi_X^{0} \left( a + a^{\dagger} \right)\\
    \hat{\varphi_Y} = \varphi_Y^{0} \left( b + b^{\dagger} \right)\\
    \hat{\varphi_Z} = \varphi_Z^{0} \left( c + c^{\dagger} \right)\\
\end{array}\right.
\end{equation}


Imposing $\omega_a = 4 \omega_c - \omega_b$, we discard everything but $a b
{c^{\dagger}}^4$ (or hermitian conjugate) terms, in the RWA. Then,

\begin{equation}
    \sin \left( \frac{\varphi_X}{2} \right) \sin \left( \frac{\varphi_Y}{2} \right) \left[ \cos \varphi_Z - \sin \varphi_Z \right] = \frac{1}{96} \varphi_X^0 \varphi_Y^0 {\varphi_Z^0}^4 \left[ a b {c^{\dagger}}^4 + a^{\dagger} b^{\dagger} c^4 \right]
\end{equation}

Finally, the effective non-linear part of the hamiltonian writes:

\begin{equation}
    H_{eff} = -\frac{\sqrt{2}}{48} E_J \varphi_X^0 \varphi_Y^0 {\varphi_Z^0}^4 \left[ a b {c^{\dagger}}^4 + a^{\dagger} b^{\dagger} c^4 \right]
\end{equation}


*Note*: This is only a quick study of the modes of the system, and a deeper
analysis is to be done, in the following sections.


### Normal modes of the system

Actually, $\varphi_X$ and $\varphi_Y$ are no longer normal modes for this
distorted system, due to the presence of a $\varphi_X \varphi_Y$ term in the
Josephson part.

The new quadratic part writes
\begin{equation}
\frac{E_L}{4} \left[ \varphi_X^2 + \varphi_Y^2 + 2\varphi_Z^2 \right] - 2\sqrt{2} E_J \varphi_X \varphi_Y
\end{equation}

Writing it as $\begin{psmallmatrix} \varphi_X & \varphi_Y & \varphi_Z \end{psmallmatrix} L \begin{psmallmatrix} \varphi_X \\
\varphi_Y \\ \varphi_Z \end{psmallmatrix}$, and diagonalizing the $L$ matrix, one
finds that the new eigenmodes are (in the particular case where $\sqrt{2}E_J =
\frac{E_L}{4}$)

\begin{equation}
\left\{ \begin{array}{l}
    \varphi_x = \varphi_X - \varphi_Y \\
    \varphi_y = \varphi_X + \varphi_Y \\
    \varphi_z = \varphi_Z
\end{array} \right. .
\end{equation}

Then,

\begin{equation}
\left\{ \begin{array}{l}
    2 \varphi_X = \varphi_x + \varphi_y \\
    2 \varphi_Y = \varphi_x - \varphi_y \\
    \varphi_Z = \varphi_z
\end{array} \right. .
\end{equation}

In this basis, the hamiltonian writes
\begin{equation}
\begin{split}
H = & \left( \frac{E_L}{4} + E_J \right) \varphi_x^2 + \left( \frac{E_L}{4} - E_J \right) \varphi_y^2 + \frac{E_L}{2} \varphi_z^2 \\
    & - 2\sqrt{2} E_J \biggl[ %
            -\frac{\varphi_x + \varphi_y}{4} \frac{\varphi_x - \varphi_y}{4} \varphi_z - %
            \frac{1}{3!} \frac{\varphi_x + \varphi_y}{4} \left( \frac{\varphi_x - \varphi_y}{4} \right)^3 \\
%
        & - \frac{1}{3!} \left( \frac{\varphi_x + \varphi_y}{4} \right)^3 \frac{\varphi_x - \varphi_y}{4} - %
            \frac{1}{2} \frac{\varphi_x + \varphi_y}{4} \frac{\varphi_x - \varphi_y}{4} \varphi_z^2 \\
%
        & + \frac{1}{3!} \frac{\varphi_x + \varphi_y}{4} \left( \frac{\varphi_x - \varphi_y}{4} \right)^3 \varphi_z + %
            \frac{1}{3!} \left( \frac{\varphi_x + \varphi_y}{4} \right)^3 \frac{\varphi_x - \varphi_y}{4} \varphi_z \\
%
        & + \frac{1}{3!} \frac{\varphi_x + \varphi_y}{4} \frac{\varphi_x - \varphi_y}{4} \varphi_z^3 + %
            \frac{1}{(3!)^2} \left( \frac{\varphi_x + \varphi_y}{4} \right)^3 \left( \frac{\varphi_x - \varphi_y}{4} \right)^3 \\
%
        & + \frac{1}{5!} \left( \frac{\varphi_x + \varphi_y}{4} \right)^5 \frac{\varphi_x - \varphi_y}{4} + %
            \frac{1}{5!} \frac{\varphi_x + \varphi_y}{4} \left( \frac{\varphi_x - \varphi_y}{4} \right)^5 \\
%
        & + \frac{1}{2 \times 3!} \frac{\varphi_x + \varphi_y}{4} \left( \frac{\varphi_x - \varphi_y}{4} \right)^3 \varphi_z^2 + %
            \frac{1}{2 \times 3!} \left( \frac{\varphi_x + \varphi_y}{4} \right)^3 \frac{\varphi_x - \varphi_y}{4} \varphi_z^2 \\
%
        & + \frac{1}{4!} \frac{\varphi_x + \varphi_y}{4} \frac{\varphi_x - \varphi_y}{4} \varphi_z^4 \biggr]
\end{split}
\end{equation}


We are interested in terms expressing 4-photons transfers, of the form
$a^4 b^{\dagger}$ or its hermitian conjugate (for $a$ and $b$, the
annihilation operators of two different modes).

Then, the relevant terms in the previous hamiltonian expansion are:

\begin{equation}
\frac{1}{3!} \frac{\varphi_x + \varphi_y}{4} \left( \frac{\varphi_x - \varphi_y}{4} \right)^3 \varphi_z + %
\frac{1}{3!} \left( \frac{\varphi_x + \varphi_y}{4} \right)^3 \frac{\varphi_x - \varphi_y}{4} \varphi_z
\end{equation}

*Note*: The previously interesting term, $\frac{1}{4!} \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z^4$ now behaves as $\varphi_x^2 \varphi_z^4$ (resp. $\varphi_y^2 \varphi_z^4$) and does no longer gives the correct factors in these modes.

Finally, the four-photons process are no longer three-modes processes, but only
involves two modes, in the form:

\begin{equation}
\frac{1}{256 \times 3!} \left( \varphi_x^4 + \varphi_y^4 \right) \varphi_z
\end{equation}

At seventh order, one can find terms of the form $\varphi_x^4 \varphi_y^2
\varphi_z$.


The notion of normal modes of the system should be handled with care. There is
no such thing as normal modes for the whole system, as the nonlinear part is
not a perturbation to the quadratic part, but of the same order of magnitude.
Plus, once the JRM placed in a larger circuit with coupling inductances and
resonators, the eigenmodes are totally different, and can be imposed to be $X$
and $Y$, using good values for the inductances, and thus exciting
superpositions of $x$ and $y$ modes. Then, we will move back to the $X$ and
$Y$ basis for the rest of the study.


### Further terms in the development of $\sin(\varphi_X / 2) \sin(\varphi_Y / 2) \cos (\varphi_Z)$


We want to linearize the interesting part of the hamiltonian:
\begin{equation}
H_1 = \sin \left( \varphi_a^0 \frac{a + a^{\dagger}}{2} \right) \sin \left( \varphi_b^0 \frac{b + b^{\dagger}}{2} \right) \cos \left( \varphi_c^0 (c + c^{\dagger}) \right)
\end{equation}

Let us note $g_0 = \frac{\varphi_a^0 \varphi_b^0 {\varphi_c^0}^4}{2 \times 2 \times 4!}$.

\begin{equation}
H_1 = g_0 \left(\sum_{i\geq 0} (-1)^i \left(\varphi_a^0 / 2\right)^{2i} \frac{(a + a^{\dagger})^{2i+1}}{(2i + 1)!} \right) b \left(c^{\dagger}\right)^4
\end{equation}

We are interested in terms of the form $(a^{\dagger}a)^k a b {c^{\dagger}}^4$
(and hermitian conjugate) which are resonant in the RWA, and might be large as
$a$ is the pump mode. We denote by $H_1^1$ the expansion on
$(a^{\dagger}a)^k a b {c^{\dagger}}^4$ and we have $H_1 \approx H_1^1 + h.c.$.

\begin{equation}
H_1^1 = g_0 \left( \sum_{i \geq 0} (-1)^i (\varphi_a^0 / 2)^{2i}
\sum_{\substack{k=0 \\ k = 1 [2]}}^{2i+1} \left( \frac{-[a, a^{\dagger}]}{2}\right)^{(2i + 1 -k) / 2} \times \frac{1}{k! \left( \frac{2i + 1 -k}{2}\right)!}
\sum_{r=0}^k \binom{k}{r} \left( a^{\dagger} \right)^r a^{k-r}
\right) b \left(c^{\dagger}\right)^4
\end{equation}

We only keep terms with the same power in $a$ and $a^{\dagger}$ and ending in
$a b \left(c^{\dagger}\right)^4$ (resonant in the RWA).


\begin{equation}
H_1^1 = g_0 \left( \sum_{i \geq 0} (-1)^i (\varphi_a^0 / 2)^{2i}
\sum_{\substack{k=0 \\ k = 1 [2]}}^{2i+1} \left( \frac{1}{2}\right)^{i - (k - 1) / 2} \times \frac{1}{k! \left( i - \frac{k -1}{2}\right)!}
\binom{k}{\frac{k-1}{2}} \left(a^{\dagger}\right)^{(k-1) / 2} a^{(k-1) / 2}
\right) a b \left(c^{\dagger}\right)^4
\end{equation}

We introduce $p$ such that $k = 2p +1$,
\begin{equation}
H_1^1 = g_0 \left( \sum_{i \geq 0} (-1)^i (\varphi_a^0 / 2)^{2i}
    \sum_{p \geq 0}^i \frac{2^{-i} 2^p}{(i - p)! p! (p+1)!}
\left(a^{\dagger}\right)^{p} a^{p}
\right) a b \left(c^{\dagger}\right)^4
\end{equation}

\begin{equation}
H_1^1 = g_0 \left( \sum_{p \geq 0} \left(
    \sum_{i \geq p} \frac{(-(\varphi_a^0)^2 / 8)^{i}}{(i-p)!}
    \right)
    \frac{2^p}{p! (p+1)!}
    \frac{(a^{\dagger}a)!}{(a^{\dagger}a - p)!}
\right) a b \left(c^{\dagger}\right)^4
\end{equation}

\begin{equation}
H_1^1 = g_0 e^{-(\varphi_a^0)^2 / 8} \left( \sum_{p \geq 0}
    \frac{(-(\varphi_a^0)^2 / 8)^p \times 2^p}{p! (p+1)!}
    \frac{(a^{\dagger}a)!}{(a^{\dagger}a - p)!}
\right) a b \left(c^{\dagger}\right)^4
\end{equation}

\begin{equation}
H_1^1 = g_0 e^{-(\varphi_a^0)^2 / 8} \left( \sum_{p \geq 0}
    \frac{(-(\varphi_a^0)^2 / 4)^p}{(p+1)!}
    \binom{a^{\dagger}a}{p}
\right) a b \left(c^{\dagger}\right)^4
\end{equation}

\begin{equation}
H_1^1 = g_0 e^{-(\varphi_a^0)^2 / 8} \left( \sum_{p \geq 1}
    \frac{(-(\varphi_a^0)^2 / 4)^p}{p!}
    \binom{(a^{\dagger}a + 1) - 1}{(a^{\dagger}a +1) - p}
    \left( - \frac{(\varphi_a^0)^2}{4} \right)^{-1}
\right) a b \left(c^{\dagger}\right)^4
\end{equation}

\begin{equation}
\boxed{H_1^1 = \left[
- \frac{\varphi_a^0}{2} e^{-(\varphi_a^0)^2 / 8}
\frac{L_{a^{\dagger}a + 1}^{(-1)} \left((\varphi_a^0)^2 / 4 \right)}{(\varphi_a^0)^2 / 4}
\right] \frac{\varphi_b^0}{2} \frac{(\varphi_c^0)^4}{4!} ab (c^{\dagger})^4}
\end{equation}

and
\begin{equation}
\boxed{\left(H_1^1\right)^{\dagger} =
\left[
- \frac{\varphi_a^0}{2} e^{-(\varphi_a^0)^2 / 8}
\frac{L_{a^{\dagger}a}^{(-1)} \left((\varphi_a^0)^2 / 4 \right)}{(\varphi_a^0)^2 / 4}
\right] \frac{\varphi_b^0}{2} \frac{(\varphi_c^0)^4}{4!} a^{\dagger} b^{\dagger} c^4
}
\end{equation}


In the case of a coherent state, the prefactor becomes,
\begin{equation}
\begin{split}
&\sum_{p \geq 0} \sum_{i \geq p}
\frac{(-(\varphi_a^0)^2/8)^i}{\left( i - p\right)!}
\frac{2^p}{p! (p+1)!}
{a^{\dagger}}^{p}a^{p} \\
%
& = e^{-\left(\varphi_a^0\right)^2 / 8} \sum_{p \geq 0} \frac{(-1)^p \left(
(\varphi_a^0)^2 |\alpha|^2 / 4\right)^{p}}{p! (p+1)!} \\
%
& = e^{-\left(\varphi_a^0\right)^2 / 8} \sum_{p \geq 0} \frac{(-1)^p}{p! \Gamma(p + 1 + 1)} \frac{\left(\varphi_a^0 |\alpha| / 2\right)^{2p + 1}}{\left(\varphi_a^0 |\alpha| / 2\right)}\\
%
& = e^{-\left(\varphi_a^0\right)^2 / 8}
    \times \frac{J_1 \left( \varphi_a^0 |\alpha| \right)}{\left(\varphi_a^0 |\alpha| / 2\right)}
\end{split}
\end{equation}

And then, the hamiltonian becomes
\begin{equation}
\boxed{\begin{split}
H = & \left[\frac{\varphi_a^0}{2} \times
    e^{-\left(\varphi_a^0\right)^2 / 8} \times
    \frac{J_1\left(\varphi_a^0 |\alpha|\right)}{\left( \varphi_a^0 |\alpha| \right) / 2}
    \right]
\times \frac{\varphi_b^0}{2} \frac{(\varphi_c^0)^4}{4!} a b {c^{\dagger}}^4 +
h.c.
\end{split}}
\end{equation}

For the optimal value of $|\alpha|^2$, we have approximatively
\begin{equation}
\frac{\varphi_a^0}{2} \times e^{-\left( \varphi_a^0 \right)^2 / 8} \times \frac{J_1 \left(\varphi_a^0 |\alpha|\right)}{\left( \varphi_a^0 |\alpha| \right) / 2} \times a \approx 0.5
\end{equation}
as $a \approx |\alpha|$.

*Note*: $\sin \left( \varphi_a^0 \frac{a + a^{\dagger}}{2} \right) \approx 1$
for this optimal value. However, the factor 2 we have comes from the hermitian
conjugate part.


#### Optimal value for $|\alpha|$ as a function of $\varphi_a^0$

We want
\begin{equation}
\frac{\varphi_a^0}{2} \times e^{-\left( \varphi_a^0 \right)^2 / 8} \times \frac{J_1 \left(\varphi_a^0 |\alpha|\right)}{\left( \varphi_a^0 |\alpha| \right) / 2} |\alpha|
\end{equation}
to be maximal.

This is equivalent to
\begin{equation}
J_1 \left(\varphi_a^0 |\alpha|\right)
\end{equation}
maximal.


\begin{equation}
\frac{\textrm{d}}{\textrm{d} |\alpha|} \left(\varphi_a^0 |\alpha|\right) =
\varphi_a^0 \left( J_0 \left(\varphi_a^0 |\alpha| \right) - \frac{1}{\varphi_a^0 |\alpha|} J_1 \left(\varphi_a^0 |\alpha| \right) \right)
\end{equation}


We then have
\begin{equation}
\boxed{
|\alpha|_{\text{optimal}}^2 = \left[
    \frac{\textrm{FindRoot} \left[ J_1(x) == x \times J_0(x)\right]}{\varphi_a^0}
\right]^2
}
\end{equation}

We have $\textrm{FindRoot} \left[ J_1(x) == x \times J_0(x)\right] \approx 1.84118$.
Then,
\begin{equation}
|\alpha|_{\text{optimal}}^2 \approx \left[ \frac{1.841\dots}{\varphi_a^0} \right]^2
\end{equation}

![Optimal number of photons in the pump, $|\alpha|^2$, as a function of $\varphi_a^0$.](figures/optimal_pump_number_photons.svg)


#### Expression in terms of displacement operators

\begin{equation}
    \langle \alpha | \sin ((a + a^{\dagger}) / 2) | \alpha \rangle = \Im \langle \alpha | e^{i (a + a^{\dagger}) / 2} | \alpha \rangle
\end{equation}

We have
\begin{equation}
    e^{i (a + a^{\dagger}) / 2} | \alpha \rangle = D(i / 2) D(\alpha) | 0 \rangle
\end{equation}

given that
\begin{equation}
    D(\alpha) = \exp (\alpha a^{\dagger} - \alpha^* a)
\end{equation}


Using
\begin{equation}
    D(\alpha) D(\beta) = e^{(\alpha \beta^* - \alpha^* \beta) / 2} D(\alpha + \beta)
\end{equation}

we have
\begin{equation}
    e^{i (a + a^{\dagger}) / 2} | \alpha \rangle = e^{i \Re(\alpha) / 2} | \alpha + i/2 \rangle
\end{equation}

and finally
\begin{equation}
    \langle \alpha | e^{i (a + a^{\dagger}) / 2} | \alpha \rangle = e^{i \Re(\alpha) / 2} \langle \alpha | \alpha + i / 2 \rangle
\end{equation}

\begin{equation}
    \langle \alpha | e^{i (a + a^{\dagger}) / 2} | \alpha \rangle = e^{i \Re(\alpha) / 2} \exp \left( \frac{|\alpha|^2 - |\alpha + i / 2|^2}{2} + i \frac{\alpha^*}{2} \right)
\end{equation}

\begin{equation}
    \langle \alpha | \sin ((a + a^{\dagger}) / 2) | \alpha \rangle = \exp \left( \frac{|\alpha|^2 - |\alpha + i / 2|^2}{2} \right) \sin \left( \frac{\Re(\alpha)}{2} + \frac{\alpha^*}{2} \right)
\end{equation}


\pagebreak

### RWA estimation

See Mathematica notebook `RWA.nb`.

#### Hamiltonian expansion

Up to $O(\|\varphi\|^7)$, we have
\begin{equation}
\begin{split}
    \sin \left( \frac{\varphi_X}{2} \right) \sin \left( \frac{\varphi_Y}{2} \right) \left[ \cos \varphi_Z - \sin \varphi_Z \right] = & %
            \frac{\varphi_X}{2} \frac{\varphi_Y}{2} - %
            \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z - %
            \frac{1}{3!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^3 \\
%
        & - \frac{1}{3!} \left( \frac{\varphi_X}{2} \right)^3 \frac{\varphi_Y}{2} - %
            \frac{1}{2} \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z^2 \\
%
        & + \frac{1}{3!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^3 \varphi_Z + %
            \frac{1}{3!} \left( \frac{\varphi_X}{2} \right)^3 \frac{\varphi_Y}{2} \varphi_Z \\
%
        & + \frac{1}{3!} \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z^3 + %
            \frac{1}{(3!)^2} \left( \frac{\varphi_X}{2} \right)^3 \left( \frac{\varphi_Y}{2} \right)^3 \\
%
        & + \frac{1}{5!} \left( \frac{\varphi_X}{2} \right)^5 \frac{\varphi_Y}{2} + %
            \frac{1}{5!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^5 \\
%
        & + \frac{1}{2 \times 3!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^3 \varphi_Z^2 + %
            \frac{1}{2 \times 3!} \left( \frac{\varphi_X}{2} \right)^3 \frac{\varphi_Y}{2} \varphi_Z^2 \\
%
        & + \frac{1}{4!} \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z^4
\end{split}
\end{equation}

In the following, we assume we can drive each mode separately, and we associate operators $a$, $b$ and $c$ to respectively $\varphi_X$, $\varphi_Y$ and $\varphi_Z$. Furthermore, we impose $\omega_a = 4 \omega_c - \omega_b$ for the mode frequencies ($a$ being the pump mode).

The total hamiltonian is
\begin{equation}
\begin{split}
H & = \underbrace{\hbar \omega_a a^{\dagger}a + \hbar \omega_b b^{\dagger} b + \hbar \omega_c c^{\dagger} c}_{H_0} \\
    & - 2 \sqrt{2} E_J \sin \left( \frac{\left(a + a^{\dagger} \right)}{2} \right) \sin \left( \frac{\left(b + b^{\dagger} \right)}{2} \right) \left[ \cos \left(c + c^{\dagger}\right) - \sin \left(c + c^{\dagger}\right) \right]
\end{split}
\end{equation}


#### 1st order

Using RWA to first order, the evolution in the rotating frame associated with
$H_0$ is given by
\begin{equation}
H = \frac{-\sqrt{2} E_J}{48} \varphi_a^0 \varphi_b^0 \left(\varphi_c^0\right)^4 a b {c^{\dagger}}^4 %
    + h.c.
\end{equation}

*Note*: There is a factor 16 between this expression and the one in Mirrahimi
et al., NJP, due to the different normalization of the $z$ mode. Note also
that $a$, $b$ and $c$ do not correspond to the same modes.


### 2nd order

There are too many terms at this order. However, a basic computation is made
in the Mathematica notebook.

\pagebreak


## Evaluation of the interaction term in the hamiltonian

### Symmetric case

In the symmetric case, for a JRM with resonators such as Fig. 14 from
_Flurin thesis_, we have

\begin{equation}
    H = H_0 + H_{\text{3WM}}
\end{equation}

where
\begin{equation}
    H_0 = \hbar \omega_a a^{\dagger} a +
          \hbar \omega_b b^{\dagger} b +
          \hbar \omega_c c^{\dagger} c
\end{equation}

and (taking $\varphi_{\text{ext}} = 2\pi$)
\begin{equation}
    H_{\text{3WM}} = - \frac{1}{\sqrt{2}} E_J \varphi_a \varphi_b \varphi_c
\end{equation}

$\varphi_{a, b, c}$ being the node fluxes at the JRM nodes.

These node fluxes can be expressed in terms of the associated node fluxes
after the antennas, $\Phi_{a,b,c}$ as

\begin{equation}
    \Phi_{a, b, c} = \varphi_0 \varphi_{a, b, c} / \xi_{a, b, c}
\end{equation}

where $\xi_{a, b, c}$ are the participation factors,

\begin{equation}
    \xi_{a, b, c} = L_{a, b, c}^{\text{JRM}} / L_{a, b, c}^{\text{tot}}
\end{equation}

where $L_{a, b, c}^{\text{JRM}}$ is the inductive part associated to each mode
in the JRM hamiltonian and $L_{a, b, c}^{\text{tot}}$ is the sum of the
previous contribution and the inductances from the antennas.

When quantizing the fluxes, we have
\begin{equation}
    \Phi_{X = a, b, c} = \sqrt{\frac{\hbar Z_{a, b, c}}{2}} (X + X^{\dagger})
\end{equation}
where $Z_{a, b, c} = L_{a, b, c}^{\text{tot}} \omega_{a, b, c}$.


We want to write $H_{\text{3WM}} = \hbar \chi (b + b^{\dagger}) (c + c^{\dagger})$ and to evaluate the order of magnitude of $\chi$.

Treating the pump ($a$ mode) semi-classically, and by pushing it enough, we
can estimate the first sin to be of order 1. Then,
\begin{equation}
    \chi \propto \frac{E_J}{\hbar} \xi_b \xi_c \sqrt{\frac{\hbar^2 Z_b Z_c}{4 \varphi_0^4}}
\end{equation}

\begin{equation}
    \chi \propto \frac{E_J}{\hbar} \sqrt{\xi_b \xi_c} \sqrt{\frac{\hbar^2 L_b^{\text{JRM}} L_c^{\text{JRM}}}{4 \varphi_0^4}} \sqrt{\omega_b \omega_c}
\end{equation}

We have $L_{b, c}^{\text{JRM}} \propto L$ with $L$ the shunt inductance. Then,
\begin{equation}
    \chi \propto \frac{E_J}{E_L} \sqrt{\xi_b \xi_c} \sqrt{\omega_b \omega_c}
\end{equation}

And we have typically $\omega_b \approx \omega_c \approx 10^{10} Hz$, $E_J \approx E_L$ and
$\xi_b \approx \xi_c \approx 0.1$, which gives $\chi \approx 10^9$.


### Asymmetric case

Doing the same calculation in the asymmetric case gives us

\begin{equation}
    H = H_0 + \hbar \chi (a b (c^{\dagger})^4 + h.c.)
\end{equation}

where
\begin{equation}
    \chi \propto \frac{E_J}{E_L^{5/2}} \sqrt{\xi_b \xi_c^4} \sqrt{\omega_b \omega_c^4}
\end{equation}

that is
\begin{equation}
    \chi \propto \frac{E_J}{E_L} \sqrt{\xi_b \xi_c} \sqrt{\omega_b \omega_c}
                 \left( \frac{\xi_c \omega_c}{E_L / \hbar} \right)^{3/2}
\end{equation}
where $\propto$ means "up to a numerical prefactor".

The first term is the one from the symmetric case, typically of the order of
$10^9$. For shunt inductances of $30pH$ as is done by _Flurin_, we get $E_L
\approx 10^{15}$. The term to the power $3/2$ is of the order $(10^{-1} \times
10^{10} / 10^{15})^{(3/2)} = 10^-9$, leading to $\chi \approx 1Hz$ which is
really small. Taking larger values and trying to optimize a bit, we can reach
$\chi \approx \text{a few } MHz$, but we should take care to stay in the same
operating regime.

*Note*: Convergence of the cat states is in $P(\alpha) \chi$ where $\alpha$ is
the coherent state in the pump and $P$ is a sixth-order polynomials. For
$\alpha \approx 3$, $P(\alpha) \approx 1000$. See _Azouit_ and _Six_.


### Full expansion on the $c$ mode

From the above equations, we see that the coupling is really weak due to the
presence of $\varphi_c^0$ to the power 4. An idea would be to increase
$\varphi_c^0$, but this will most likely introduce further terms in the
hamiltonian. In particular, terms of the form $(c^{\dagger}c)^k a b
(c^{\dagger})^4$ will most likely not be negligible anymore. Let us try to
estimate these with an approach similar to the one used to expand on the
powers of the pump mode.

\begin{equation}
\begin{split}
\cos \left[ \varphi_c^0 \left( c + c^{\dagger} \right)\right] & =
    \sum_{k \geq 0} (-1)^k \frac{\left( \varphi_c^0 \right)^{2k}}{(2k)!} \left( c + c^{\dagger} \right)^{2k} \\
%
    & = \sum_{k \geq 0} \frac{(- (\varphi_c^0)^2)^k}{(2k)!} \left( c + c^{\dagger} \right)^{2k} \\
%
    & = \sum_{k \geq 0} \frac{(- (\varphi_c^0)^2)^k}{\cancel{(2k)!}} \sum_{\substack{i = 0 \\ i = 0 [2]}}^{2k} \left( - \frac{[c^{\dagger}, c]}{2} \right)^{k - i/2} \frac{\cancel{(2k)!}}{i! (k - i/2)!} \sum_{r = 0}^i \binom{i}{r} (c^{\dagger})^r c^{i - r} \\
%
    & = \sum_{k \geq 0} (- (\varphi_c^0)^2)^k \sum_{p = 0}^k \left( \frac{1}{2} \right)^{k - p} \frac{1}{(2p)! (k - p)!} \sum_{r = 0}^{2p} \binom{2p}{r} (c^{\dagger})^r c^{2p - r} \\
%
\end{split}
\end{equation}

We only keep terms ending in $c^4$ and hermitian conjugate (resonant in the RWA) with equal number of leading $c$ and $c^{\dagger}$.

\begin{equation}
\begin{split}
\cos \left[ \varphi_c^0 \left( c + c^{\dagger} \right)\right] & =
    \left[\sum_{k \geq 0} ( - (\varphi_c^0)^2)^k \sum_{p = 2}^k \frac{2^{p-k}}{(2p)! (k-p)!} \binom{2p}{p-2} (c^{\dagger})^{p-2} c^{p-2} \right] c^4 + h.c. \\
%
& = \left[\sum_{p \geq 2} \left( \sum_{k \geq p} \frac{(- (\varphi_c^0)^2 / 2)^k}{(k-p)!} \right)
    \frac{2^p}{(p-2)! (p+2)!} (c^{\dagger})^{p-2} c^{p-2}\right] c^4 + h.c. \\
%
& = e^{-(\varphi_c^0)^2 / 2} \left[\sum_{p \geq 2}
    \frac{(-(\varphi_c^0)^2)^p}{(p-2)! (p+2)!} (c^{\dagger})^{p-2} c^{p-2}\right] c^4 + h.c. \\
%
& = e^{-(\varphi_c^0)^2 / 2} \left[\sum_{p \geq 0}
    \frac{(-(\varphi_c^0)^2)^{p + 2}}{p! (p+4)!} (c^{\dagger})^p c^p\right] c^4 + h.c. \\
%
& = (\varphi_c^0)^4 \times e^{-(\varphi_c^0)^2 / 2} \left[\sum_{p \geq 0}
    \frac{(-1)^p}{p! (p+4)!} (\varphi_c^0)^{2p} (c^{\dagger})^p c^p\right] c^4 + h.c. \\
%
\end{split}
\end{equation}

When comparing to the standard expansion $(\varphi_c^0)^4 c^4 / 4! + h.c.$, we
have a multiplicator

\begin{equation}
G = 4! \times e^{-(\varphi_c^0)^2 / 2} \left[\sum_{p \geq 0}
    \frac{(-1)^p}{p! (p+4)!} (\varphi_c^0)^{2p} (c^{\dagger})^p c^p\right]
\end{equation}

Let us focus now in the case of a coherent state $| \alpha_c \rangle$.
\begin{equation}
\begin{split}
\langle \alpha_c | G | \alpha_c \rangle = & 4! \times e^{-(\varphi_c^0)^2 / 2} \left[\sum_{p \geq 0}
    \frac{(-1)^p}{p! (p+4)!} (\varphi_c^0)^{2p} |\alpha_c|^{2p} \right] \\
%
& = 24 e^{-(\varphi_c^0)^2 / 2} \left[\sum_{p \geq 0}
    \frac{(-1)^p}{p! (p+4)!} \left(\frac{2 \varphi_c^0 |\alpha_c|}{2}\right)^{2p} \right] \\
%
& = 24 e^{-(\varphi_c^0)^2 / 2} \times \frac{J_4\left(2 \varphi_c^0 |\alpha_c|\right)}{\left(\varphi_c^0 |\alpha_c|\right)^4} \\
\end{split}
\end{equation}

\begin{equation}
\boxed{\langle \alpha_c | G | \alpha_c \rangle = 24 e^{-(\varphi_c^0)^2 / 2} \times \frac{J_4\left(2 \varphi_c^0 |\alpha_c|\right)}{\left(\varphi_c^0 |\alpha_c|\right)^4}}
\end{equation}

By increasing the value of $\varphi_c^0$, we can tweak the value of the
coupling constant up to some tens of MHz. See `RWA.ipynb` notebook for extra
graphs and information on this.


## Optimizing the coupling and convergence rate

As shown in previous sections, $\chi$ is very small and needs to be increased,
at least to reach the regime in which $\kappa_{\text{4ph}} >
\kappa_{\text{1ph}}$.

Taking into accounts the symmetry, we can tweak the following parameters:

- $E_J$ of the Josephson junctions, but we would rather keep this one
constant.
- $L$ the shunt inductances.
- $L_a$ and $L_b$, the external inductances.


An easy tweak is to set $L_a$ and $L_b$ such that the participation ratios are
1/2. This is achieved for $\boxed{L_a = L_a^{\text{JRM}} = 2L}$ and
$\boxed{L_b = L_b^{\text{JRM}} = 2L}$.

We then want to increase the shunt inductances to an acceptable value
(experimentally doable) to increase $\chi$.

We have
\begin{equation}
    \chi = \frac{E_J}{E_L} \sqrt{\xi_b \xi_c} \sqrt{\omega_b \omega_c}
                 \left( \frac{\xi_c \omega_c}{E_L / \hbar} \right)^{3/2}
\end{equation}

\begin{equation}
    \chi = \frac{E_J \sqrt{\xi_b \xi_c} \sqrt{\omega_b \omega_c}
    \left(\xi_c \hbar \omega_c \right)^{3/2}}{{\varphi_0}^5} L^{5/2}
\end{equation}

After replacing with values from the Flurin's thesis, we get
\begin{equation}
    \boxed{\chi \approx 2.4 \times 10^{28} \times L^{5/2}}
\end{equation}

One photon loss rate, $\kappa_{\text{1ph}}$, is typically of the order of a
few tens of kHz. We would like to have $\chi \approx 1$ MHz. This means that
we should have \fbox{$L \approx 1$ nH}. (Actually, we have
$\chi \approx 4.6 \times 10^5$ Hz for this value, taking into account the full
expansion, with $|\alpha_c| = 2$).

### Convergence rate

By performing adiabatic elimination of mode $b$ (see _Carmichael_), we have
something like

\begin{equation}
\dot{\rho} = \left[
    \varepsilon_{4ph} \left(a^{\dagger}\right)^4 -
    \varepsilon_{4ph}^* a^4 \right] +
    \kappa_{4ph} D[a^4] \rho
\end{equation}
where $\varepsilon_{4ph} = \frac{2 \varepsilon_b g_{4ph}}{\kappa_b}$,
$\kappa_{4ph} = \frac{4 g_{4ph}^2}{\kappa_b}$ and
$\alpha_{\text{steady}} = \left(\frac{2 \varepsilon_{4ph}}{\kappa_{4ph}}\right)^{1/4}$.

See `RWA.ipynb` notebook for extra graphs and information on this.

When starting from $\left|0\right\rangle$, we have a convergence in
approximately $4!\, \kappa_{4ph}$.

When starting from a coherent state, convergence rate is much higher, up to
$16|\alpha|^6 + 24|\alpha|^4 + 56|\alpha| + 24 \approx 10^3$ (for $|\alpha|^2 =
4$).


### Dealing with perturbations around the stable point

We would like to estimate the convergence rate for perturbations around the
stable span of cat states. These are typically dephasing of the cat states
(replacing $\alpha$ by $\alpha e^{i \varphi}$) or damping (replacing $\alpha$
by $\alpha e^{-\kappa}$).

The convergence rate in such cases is given by $P(|\alpha|^2)
\kappa_{4ph}(\alpha)$ where
$P(x) = (x+1)(x+2)(x+3)(x+4) - x(x-1)(x-2)(x-3)$ and $\alpha \approx
\alpha_{\text{steady}}$.

Taking into account all the dependencies, we get that this rate is
proportional to $-|\alpha|^2$. See `RWA.ipynb` notebook for extra graphs and
information on this.


## Fixing normal modes by tweaking shunt inductances

If we go to the regime in which $L_a = L_b = 2L$, the external inductances are
of the same order of magnitude as the shunt ones. Then, the external
inductances no longer imposes the "normal" modes of the system, which are
obtained from the quadratic part of the hamiltonian (see previous section).

Let us try to impose again the same modes by tweaking the shunt
inductances. We replace the identical inductances $L$, by four different
inductances, $L_1$, $L_2$, $L_3$ and $L_4$.

Then, the quadratic part of the hamiltonian is
\begin{equation}
    H_1 = E_{L_1} \frac{{\varphi_1}^2}{2} +
          E_{L_2} \frac{{\varphi_2}^2}{2} +
          E_{L_3} \frac{{\varphi_3}^2}{2} +
          E_{L_4} \frac{{\varphi_4}^2}{2} -
          2 \sqrt{2} E_J \varphi_x \varphi_y
\end{equation}
which we can rewrite
\begin{equation}
    H_1 = \frac{E_L}{2} \left[
        {\varphi_1}^2 +
        \alpha \times {\varphi_2}^2 +
        \beta \times {\varphi_3}^2 +
        \gamma \times {\varphi_4}^2\right] -
        4 C \varphi_x \varphi_y]
\end{equation}
where $\alpha$, $\beta$ and $\gamma$ are constants depending on the ratios of
the shunt inductances and $C$ is a proportionnality constant.

Recalling that $\varphi_1 = \frac{\varphi_y + \varphi_z}{2}$,
$\varphi_2 = \frac{\varphi_z - \varphi_y}{2}$,
$\varphi_3 = \frac{\varphi_x - \varphi_z}{2}$ and
$\varphi_4 = -\frac{\varphi_x + \varphi_z}{2}$,

we have
\begin{equation}
    H_1 = \frac{E_L}{8} \left[
        \beta (\gamma + 1) {\varphi_x}^2 +
        (\alpha + 1) {\varphi_y}^2 +
        (2 - 2 \alpha) \varphi_y \varphi_z +
        \beta (2 \gamma - 2) \varphi_x \varphi_z +
        C \varphi_x \varphi_y +
        \delta {\varphi_z}^2\right]
\end{equation}
where $\delta$ is a constant depending on $\alpha$, $\beta$ and $\gamma$.

We can assume $\alpha = 1$ and $\gamma = 1$ to simplify further the
expression and removing cross terms, leading to
\begin{equation}
    H_1 = \frac{E_L}{4}\left[
        \beta {\varphi_x}^2 + {\varphi_y}^2 + C' \varphi_x \varphi_y +
        \delta' {\varphi_z}^2\right]
\end{equation}

Factoring this polynomials we get two modes which are
$\varphi_X = \frac{1}{4\beta} \left( 2 \beta \varphi_x + C' \varphi_y \right)^2$ and
$\varphi_Y = \frac{(4\beta - C'^2)}{4\beta}$.


To recover the previous normal modes, we want $\boxed{4 \beta - C'^2 > 0}$ and
$\boxed{2 \beta |\varphi_x| \gg C' |\varphi_y|}$.


\pagebreak

## Some other designs

### 2 triangles geometry

![2 triangles geometry](figures/two_triangles.svg)

Writing circuit equations, we have $\varphi_a = \varphi_1$ and $\varphi_b =
\varphi_2$.

Hamiltonian is
\begin{equation}
H = -E_J \cos \varphi_X \cos \varphi_Y + \frac{E_L}{2} \left( \varphi_X^2 +
\varphi_Y^2 \right).
\end{equation}

where $\varphi_X = \frac{\varphi_1 - \varphi_2}{2}$ and $\varphi_Y = \frac{\varphi_1 - \varphi_2}{2}$.

This circuit is not really interesting as it only has two modes and inputs.


### 6 loops geometry

Using 6 loops instead of 3 loops, one has 5 "normal" modes. However most of
them are coupled and difficult to excite individually using only one or two
probes.
